#version 330

// From OpenFrameworks
uniform mat4 modelMatrix;
uniform mat4 modelViewProjectionMatrix;
uniform mat4 modelViewMatrix;

// Transform Matrix to convert vertex position from camera view space to light cam view space
uniform mat4 uShadowTransMatrix0;
uniform mat4 uShadowTransMatrix1;

// From OpenFrameworks
in vec4 position;
in vec4 color;
in vec2 texcoord;

out vec4 vVertPosInLightSpace0; // Vertex Position in Light cam View Space
out vec4 vVertPosInLightSpace1; // Vertex Position in Light cam View Space
out vec2 vTexCoord;  // Texture coordinate from object

void main(void)
{
    vec4 vertInViewSpace = modelViewMatrix * position;
    vVertPosInLightSpace0 = uShadowTransMatrix0 * vertInViewSpace;
    vVertPosInLightSpace1 = uShadowTransMatrix1 * vertInViewSpace;

    gl_Position = modelViewProjectionMatrix * position; // Vertex Position in Camera View Space
    vTexCoord = texcoord; // Texture Coordinate
}