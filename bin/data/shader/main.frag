#version 330

// Shadow Calculation
uniform sampler2D uShadowMap0;		// Depth map
uniform sampler2D uShadowMap1;
uniform float uShadowBias0;			// against the shadow acne
uniform float uShadowBias1;
uniform float uShadowIntensity0;
uniform float uShadowIntensity1;

in vec4 vVertPosInLightSpace0;
in vec4 vVertPosInLightSpace1;

// Texture data
in vec2 vTexCoord;
uniform sampler2D tex0;

uniform vec4 globalColor;   // Object Color
out vec4 outputColor;   // Pixel Color


// Returns value between 0 - 1(strength of the shadow); 1 Max Shadow, 0 No Shadow
float GetShadow(vec4 vertInLightSpace, sampler2D shadowMap, float shadowBias, float shadowIntensity)
{
    // If Behind Shadow View
    if( vertInLightSpace.z < 1.0)
    {
        return 0.0;
    }
    // Get projected shadow value
    vec3 tDepth = vertInLightSpace.xyz / vertInLightSpace.w;
    vec4 depth  = vec4(tDepth.xyz, vertInLightSpace.w );
    
    depth.y = 1.0 - depth.y;
    
    float shadow = 0.0;
    vec2 UVCoords = depth.xy;
    
    float texel = texture(shadowMap, UVCoords.xy).r;
    if(texel < depth.z - shadowBias)
    {
        shadow += shadowIntensity;
    }
    shadow = clamp(shadow, 0.0, 1.0 );
    
    return shadow;
}

void main()
{
    outputColor = texture(tex0, vTexCoord);
    float shade0 = 1 - GetShadow(vVertPosInLightSpace0, uShadowMap0, uShadowBias0, uShadowIntensity0);
    float shade1 = 1 - GetShadow(vVertPosInLightSpace1, uShadowMap1, uShadowBias1, uShadowIntensity1);
    outputColor.rgb = outputColor.rgb * globalColor.rgb;
    outputColor.rgb = outputColor.rgb * shade0 * shade1;
}