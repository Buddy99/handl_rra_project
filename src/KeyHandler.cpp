#include "KeyHandler.h"

KeyHandler::KeyHandler()
{
	for (int i = 0; i < NUM_KEYS; i++)
	{
		mKeyDown[i] = false;
		mKeyPressed[i] = false;
		mKeyReleased[i] = false;
	}
}
KeyHandler::~KeyHandler()
{
}

void KeyHandler::KeyPressed(int key)
{
	mKeyPressed[key] = true;
	mKeyDown[key] = true;
	mKeyReleased[key] = false;
}

void KeyHandler::KeyReleased(int key)
{
	mKeyPressed[key] = false;
	mKeyDown[key] = false;
	mKeyReleased[key] = true;
}

void KeyHandler::FrameUpdate()
{
	for (int i = 0; i < NUM_KEYS; i++)
	{
		mKeyReleased[i] = false;
		mKeyPressed[i] = false;
	}
}

bool KeyHandler::IsKeyDown(char key)
{
	return mKeyDown[key];
}

bool KeyHandler::WasKeyReleased(char key)
{
	return mKeyReleased[key];
}

bool KeyHandler::WasKeyPressed(char key)
{
	return mKeyPressed[key];
}