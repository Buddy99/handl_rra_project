#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {
	ofSetFullscreen(true);
	ofSetBackgroundColor(100, 160, 200);
	ofDisableAlphaBlending();
	ofEnableDepthTest();
	ofSetVerticalSync(true);
	ofSetTimeModeFixedRate();

	// Hide Cursor
	ofHideCursor();

	// Setup light cameras
	myShadow.SetShadowClipRange(0, 10, 1500);
	myShadow.SetShadowClipRange(1, 10, 1500);

	// Load Shader from data
	myShadow.LoadShader("shader/main");

	// Lights
	for (auto light : cornerLights)
	{
		light.enable();
	}
	cornerLights[0].setPosition(ofVec3f(50, 100, 50));
	cornerLights[1].setPosition(ofVec3f(-50, 100, 50));

	// Old lights
	//cornerLights[2].setPosition(ofVec3f(5000, 550, -5000));
	//cornerLights[3].setPosition(ofVec3f(-5000, 550, 5000));
	//cornerLights[4].setPosition(ofVec3f(5000, 550, 0));
	//cornerLights[5].setPosition(ofVec3f(-5000, 550, 0));

	// Set light cameras to light positions for correct shadow rendering
	myShadow.SetLightCamPosition(0, cornerLights[0].getPosition());
	myShadow.SetLightCamLookAt(0, ofVec3f(0, 0, 0), ofVec3f(0, 1, 0));
	myShadow.SetLightCamPosition(1, cornerLights[1].getPosition());
	myShadow.SetLightCamLookAt(1, ofVec3f(0, 0, 0), ofVec3f(0, 1, 0));

	// Textures (+ Mipmapping for some Textures)
	ofDisableArbTex();
	ofLoadImage(earthTex, "earth.png");
	ofLoadImage(grassTex, "grass.png");
	grassTex.generateMipmap();
	grassTex.setTextureMinMagFilter(GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR);
	grassTex.setTextureWrap(GL_REPEAT, GL_REPEAT);
	ofLoadImage(ornamentTex, "ornament.png");
	ofLoadImage(mipmapTex, "ornament.png");
	mipmapTex.generateMipmap();
	mipmapTex.setTextureMinMagFilter(GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR);
	ofLoadImage(cloudTex, "cloud.png");

	// Setup camera initial position
	cam.setPosition(0, 20, 0);

	// Setup ground
	ground.mapTexCoords(0, 0, 20, 20);
	ground.tiltDeg(-90);
	ground.setScale(1.5, 3.5, 1);

	setupModels();

	earth.setPosition(0, 30, -40);
	stillEarth.setPosition(-20, 30, 0);
	moveEarth.setPosition(10, 30, -10);
	earth.setScale(0.4, 0.4, 0.4);
	stillEarth.setScale(0.4, 0.4, 0.4);
	moveEarth.setScale(0.4, 0.4, 0.4);

	mousePosition = { 0,0 };
	cameraRotation = { 0,0,1 };

	initializeCameraPoints();
	d0a = ((1 - t) * (1 + c) * (1 + b)) / 2;
	d0b = ((1 - t) * (1 - c) * (1 - b)) / 2;
	d1a = ((1 - t) * (1 + c) * (1 - b)) / 2;
	d1b = ((1 - t) * (1 - c) * (1 + b)) / 2;
	lastFrame = chrono::system_clock::now();
}

//--------------------------------------------------------------
void ofApp::setupTree(Model& model, float x, float y, float z)
{
	model.lods[0].loadModel("Tree_LOD_One.obj");
	model.lods[1].loadModel("Tree_LOD_Two.obj");
	model.lods[2].loadModel("Tree_LOD_Three.obj");
	model.currentModel = model.lods[0];
	model.currentLod = 1;
	model.currentModel.setRotation(0, 180, 1, 0, 0);
	model.currentModel.setScale(0.2, 0.2, 0.2);
	model.currentModel.setPosition(x, y, z);
}

//--------------------------------------------------------------
void ofApp::setupModels()
{
	// Setup Cloud Billboards
	// Order
	int cloudPosZ = -500;
	for (int i = 0; i < numOfClouds / 4; i++)
	{
		clouds[i] = ofPlanePrimitive();
		int size = std::rand() % 1000;
		clouds[i].setWidth(50);
		clouds[i].setHeight(50);
		clouds[i].setPosition(800, 400, cloudPosZ);
		clouds[i].lookAt(cam);
		cloudPosZ += 200;
	}
	cloudPosZ = -600;
	for (int i = numOfClouds / 4; i < numOfClouds / 2; i++)
	{
		clouds[i] = ofPlanePrimitive();
		int size = std::rand() % 1000;
		clouds[i].setWidth(50);
		clouds[i].setHeight(50);
		clouds[i].setPosition(800, 550, cloudPosZ);
		clouds[i].lookAt(cam);
		cloudPosZ += 200;
	}
	// Chaos
	cloudPosZ = -500;
	for (int i = numOfClouds / 2; i < numOfClouds - numOfClouds / 4; i++)
	{
		clouds[i] = ofPlanePrimitive();
		int size = std::rand() % 1000;
		clouds[i].setWidth(50);
		clouds[i].setHeight(50);
		clouds[i].setPosition(-800 + std::rand() % 50, 400 + std::rand() % 150, cloudPosZ + std::rand() % 50);
		clouds[i].lookAt(cam);
		cloudPosZ += 200;
	}
	cloudPosZ = -600;
	for (int i = numOfClouds - numOfClouds / 4; i < numOfClouds; i++)
	{
		clouds[i] = ofPlanePrimitive();
		int size = std::rand() % 1000;
		clouds[i].setWidth(50);
		clouds[i].setHeight(50);
		clouds[i].setPosition(-800 + std::rand() % 50, 650 + std::rand() % 150, cloudPosZ + std::rand() % 50);
		clouds[i].lookAt(cam);
		cloudPosZ += 200;
	}

	// Setup transparent cubes
	// Chaos Side
	// lower
	chaosCube[0].setPosition(-40, 10, -100);
	chaosCube[0].set(10, 10);
	chaosCube[0].tiltDeg(-90);
	transparentChaosColor[0] = ofColor(0, 0, 255, 100);
	// right
	chaosCube[1].setPosition(-45, 15, -100);
	chaosCube[1].set(10, 10);
	chaosCube[1].tiltDeg(-90);
	chaosCube[1].panDeg(-90);
	transparentChaosColor[1] = ofColor(0, 255, 0, 100);
	// top
	chaosCube[2].setPosition(-40, 20, -100);
	chaosCube[2].set(10, 10);
	chaosCube[2].tiltDeg(90);
	transparentChaosColor[2] = ofColor(0, 0, 255, 100);
	// left
	chaosCube[3].setPosition(-35, 15, -100);
	chaosCube[3].set(10, 10);
	chaosCube[3].tiltDeg(-90);
	chaosCube[3].panDeg(90);
	transparentChaosColor[3] = ofColor(0, 255, 0, 100);
	// back
	chaosCube[4].setPosition(-40, 15, -105);
	chaosCube[4].set(10, 10);
	transparentChaosColor[4] = ofColor(255, 0, 0, 100);
	// front
	chaosCube[5].setPosition(-40, 15, -95);
	chaosCube[5].set(10, 10);
	transparentChaosColor[5] = ofColor(255, 0, 0, 100);

	// Order Side
	// lower
	orderCube[0].setPosition(40, 10, -100);
	orderCube[0].set(10, 10);
	orderCube[0].tiltDeg(-90);
	transparentOrderColor[0] = ofColor(100, 100);
	// right
	orderCube[1].setPosition(45, 15, -100);
	orderCube[1].set(10, 10);
	orderCube[1].tiltDeg(-90);
	orderCube[1].panDeg(-90);
	transparentOrderColor[1] = ofColor(0, 100);
	// top
	orderCube[2].setPosition(40, 20, -100);
	orderCube[2].set(10, 10);
	orderCube[2].tiltDeg(90);
	transparentOrderColor[2] = ofColor(100, 100);
	// left
	orderCube[3].setPosition(35, 15, -100);
	orderCube[3].set(10, 10);
	orderCube[3].tiltDeg(-90);
	orderCube[3].panDeg(90);
	transparentOrderColor[3] = ofColor(0, 100);
	// back
	orderCube[4].setPosition(40, 15, -105);
	orderCube[4].set(10, 10);
	transparentOrderColor[4] = ofColor(255, 100);
	// front
	orderCube[5].setPosition(40, 15, -95);
	orderCube[5].set(10, 10);
	transparentOrderColor[5] = ofColor(255, 100);

	// Setup imported models
	// Neutral
	setupTree(tree[0], -100, 0, -200);
	setupTree(tree[1], 0, 0, -300);
	setupTree(tree[2], 100, 0, -400);
	// Chaos Side
	setupTree(tree[3], -315, 0, 80);
	setupTree(tree[4], -180, 0, 50);
	setupTree(tree[5], -250, 0, 200);
	setupTree(tree[6], -330, 0, 0);
	setupTree(tree[7], -215, 0, 320);
	setupTree(tree[8], -470, 0, 300);
	setupTree(tree[9], -440, 0, 240);
	setupTree(tree[10], -390, 0, 110);
	// Order Side
	setupTree(tree[11], 400, 0, 100);
	setupTree(tree[12], 300, 0, 150);
	setupTree(tree[13], 400, 0, 200);
	setupTree(tree[14], 300, 0, 250);
	setupTree(tree[15], 400, 0, 0);
	setupTree(tree[16], 300, 0, 50);
	setupTree(tree[17], 400, 0, -100);
	setupTree(tree[18], 300, 0, -50);

	sword1.loadModel("Sword.obj");
	sword1.setRotation(0, 180, 1, 0, 0);
	sword1.setScale(0.05, 0.05, 0.05);
	sword1.setPosition(-50, 10, -45);

	sword2.loadModel("Sword.obj");
	sword2.setRotation(0, 180, 1, 0, 7);
	sword2.setScale(0.05, 0.05, 0.05);
	sword2.setPosition(-50, 10, -50);

	pillar.loadModel("S�ule.obj");
	pillar.setPosition(0, 0, -40);
	pillar.setRotation(0, 180, 1, 0, 0);
	pillar.setScale(0.025, 0.025, 0.025);

	dove.loadModel("Dove.obj");
	dove.setPosition(25, 10, -20);
	dove.setRotation(0, 180, 1, 0, 3);
	dove.setScale(0.025, 0.025, 0.025);
}

//--------------------------------------------------------------
void ofApp::initializeCameraPoints()
{
	//Camera Points

	cameraPoints.push_back({ { 0, 20, 0 }, ofQuaternion(0, ofVec3f{ 1,1,1 }) });
	cameraPoints.push_back({ { -50, 40, -50 }, ofQuaternion(45, ofVec3f{ 0,1,0 })});
	cameraPoints.push_back({ { -100, 60, -100 }, ofQuaternion(90, ofVec3f{ 0,1,0 }) });
	cameraPoints.push_back({ { -150, 80, -150 }, ofQuaternion(135, ofVec3f{ 0,1,0 }) });
	cameraPoints.push_back({ { -200, 100, -200 }, ofQuaternion(180, ofVec3f{ 0,1,0 }) });

	cameraPoints.push_back({{ -150, 120, -250 }, ofQuaternion(135, ofVec3f{ 0,1,0 }) });
	cameraPoints.push_back({{ -100, 140, -300 }, ofQuaternion(90, ofVec3f{ 0,1,0 }) });
	cameraPoints.push_back({{ -50, 160, -350 }, ofQuaternion(45, ofVec3f{ 0,1,0 }) });
	cameraPoints.push_back({{ 0, 180, -400 }, ofQuaternion(0, ofVec3f{ 0,1,0 }) });
	cameraPoints.push_back({{ 50, 160, -350 }, ofQuaternion(-45, ofVec3f{ 1,0,0 }) });

	cameraPoints.push_back({{ 100, 140, -300 }, ofQuaternion(-90, ofVec3f{ 1,0,0 }) });
	cameraPoints.push_back({{ 150, 120, -250 }, ofQuaternion(-135, ofVec3f{ 1,0,0 }) });
	cameraPoints.push_back({{ 200, 100, -200 }, ofQuaternion(-180, ofVec3f{ 1,0,0 }) });
	cameraPoints.push_back({{ 150, 80, -150 }, ofQuaternion(-135, ofVec3f{ 1,0,0 }) });
	cameraPoints.push_back({{ 100, 60, -100 }, ofQuaternion(-90, ofVec3f{ 1,0,0 }) });

	cameraPoints.push_back({{ 50, 40, -50 }, ofQuaternion(-45, ofVec3f{ 1,0,0 }) });
	cameraPoints.push_back({{ 0, 20, 0 }, ofQuaternion(0, ofVec3f{ 1,0,0 }) });
	cameraPoints.push_back({{ -50, 40, 50 }, ofQuaternion(45, ofVec3f{ 0,0,1 }) });
	cameraPoints.push_back({{ -100, 60, 100 }, ofQuaternion(90, ofVec3f{ 0,0,1 }) });
	cameraPoints.push_back({{ -150, 80, 150 }, ofQuaternion(135, ofVec3f{ 0,0,1 }) });
	cameraPoints.push_back({{ -200, 100, 200 }, ofQuaternion(180, ofVec3f{ 0,0,1 }) });

	cameraPoints.push_back({{ -150, 120, 250 }, ofQuaternion(135, ofVec3f{ 0,0,1 }) });
	cameraPoints.push_back({{ -100, 140, 300 }, ofQuaternion(90, ofVec3f{ 0,0,1 }) });
	cameraPoints.push_back({{ -50, 160, 350 }, ofQuaternion(45, ofVec3f{ 0,0,1 }) });
	cameraPoints.push_back({{ 0, 180, 400 }, ofQuaternion(0, ofVec3f{ 0,0,1 }) });
	cameraPoints.push_back({{ 50, 160, 350 }, ofQuaternion(45, ofVec3f{ 1,1,1 }) });

	cameraPoints.push_back({{ 100, 140, 300 }, ofQuaternion(90, ofVec3f{ 1,1,1 }) });
	cameraPoints.push_back({{ 150, 120, 250 }, ofQuaternion(135, ofVec3f{ 1,1,1 }) });
	cameraPoints.push_back({{ 200, 100, 200 }, ofQuaternion(180, ofVec3f{ 1,1,1 }) });
	cameraPoints.push_back({{ 150, 80, 150 }, ofQuaternion(135, ofVec3f{ 1,1,1 }) });
	cameraPoints.push_back({{ 100, 60, 100 }, ofQuaternion(90, ofVec3f{ 1,1,1 }) });

	cameraPoints.push_back({{ 50, 40, 50 }, ofQuaternion(45, ofVec3f{ 1,1,1 }) });
	cameraPoints.push_back({{ 0, 20, 0 }, ofQuaternion(0, ofVec3f{ 1,1,1 }) });
}

//--------------------------------------------------------------
CameraPoint ofApp::interpolation(int currentPoint, float k)
{
	if (cameraPoints.size() >= 0)
	{
		ofQuaternion quatSlerp;
		ofQuaternion quatSquad;

		ofQuaternion rot;
		ofQuaternion nextRot;
		ofQuaternion nextNextRot;
		ofQuaternion prevRot;

		glm::quat glmRot;
		glm::quat glmNextRot;
		glm::quat glmNextNextRot;
		glm::quat glmPrevRot;
		glm::quat intermedPoint1;
		glm::quat intermedPoint2;

		ofVec3f pos;
		ofVec3f point;
		ofVec3f prevPoint;
		ofVec3f nextPoint;
		ofVec3f nextNextPoint;

		ofVec3f d0;
		ofVec3f d1;

		int numberOfPoints = cameraPoints.size() - 1;

		// Handle edge cases
		point = cameraPoints[ofClamp(currentPoint, 0, numberOfPoints)].position;
		prevPoint = cameraPoints[ofClamp(currentPoint - 1, 0, numberOfPoints)].position;
		nextPoint = cameraPoints[ofClamp(currentPoint + 1, 0, numberOfPoints)].position;
		nextNextPoint = cameraPoints[ofClamp(currentPoint + 2, 0, numberOfPoints)].position;

		rot = cameraPoints[ofClamp(currentPoint, 0, numberOfPoints)].rotation;
		prevRot = cameraPoints[ofClamp(currentPoint - 1, 0, numberOfPoints)].rotation;
		nextRot = cameraPoints[ofClamp(currentPoint + 1, 0, numberOfPoints)].rotation;
		nextNextRot = cameraPoints[ofClamp(currentPoint + 2, 0, numberOfPoints)].rotation;

		// Kochanek-Bartels
		d0 = d0a * (point - prevPoint) + d0b * (nextPoint - point);
		d1 = d1a * (nextPoint - point) + d1b * (nextNextPoint - nextPoint);

		float ax, bx, cx, dx;
		float ay, by, cy, dy;
		float az, bz, cz, dz;
		// For x
		ax = 2.0 * point.x - 2.0 * nextPoint.x + d0.x + d1.x;
		bx = -3.0 * point.x + 3.0 * nextPoint.x - 2.0 * d0.x - d1.x;
		cx = d0.x;
		dx = point.x;
		// For y
		ay = 2.0 * point.y - 2.0 * nextPoint.y + d0.y + d1.y;
		by = -3.0 * point.y + 3.0 * nextPoint.y - 2.0 * d0.y - d1.y;
		cy = d0.y;
		dy = point.y;
		// For z
		az = 2.0 * point.z - 2.0 * nextPoint.z + d0.z + d1.z;
		bz = -3.0 * point.z + 3.0 * nextPoint.z - 2.0 * d0.z - d1.z;
		cz = d0.z;
		dz = point.z;

		float time2 = 0, time3 = 0;

		if (k != 0)
		{
			time2 = k * k;
			time3 = time2 * k;
		}
		glmRot = rot;
		glmNextRot = nextRot;
		glmNextNextRot = nextNextRot;
		glmPrevRot = prevRot;
		intermedPoint1 = glm::intermediate(glmPrevRot, glmRot, glmNextRot);
		intermedPoint2 = glm::intermediate(glmRot, glmNextRot, glmNextNextRot);
		quatSquad = glm::squad(glmRot, glmNextRot, intermedPoint1, intermedPoint2, k);
		pos = { ax * time3 + bx * time2 + cx * k + dx, ay * time3 + by * time2 + cy * k + dy, az * time3 + bz * time2 + cz * k + dz };
		return CameraPoint(pos, quatSquad);
	}
	return CameraPoint({ 0,0,0 }, { 0, {0, 0 ,0} });
}

//--------------------------------------------------------------
void ofApp::reset()
{
	currentCameraPoint = 0;
	currentDistance = 0;
	distanceToPoint.clear();
	distanceToNext.clear();

	if (cameraPoints.size() == 0)
	{
		return;
	}

	// Calculate distances for constant speed
	for (int i = 0; i < static_cast<int>(cameraPoints.size()); i++)
	{
		distanceToPoint.push_back(0);
		distanceToNext.push_back(0);
	}
	ofVec3f previousP = cameraPoints[0].position;
	for (float k = accuracy; k <= cameraPoints.size() - 1; k += accuracy)
	{
		ofVec3f currentP = interpolation((int)k, fmodf(k, 1.0)).position;
		distanceToNext[static_cast<int>(k - accuracy)] += previousP.distance(currentP);
		previousP = currentP;
	}
	for (int i = 1; i < static_cast<int>(cameraPoints.size()); i++)
	{
		for (int j = 0; j < i; j++)
		{
			distanceToPoint[i] += distanceToNext[j];
		}
	}
}

//--------------------------------------------------------------
float ofApp::doStep(float s)
{
	// Calculate distances for constant speed
	int index = -1;
	currentDistance += s;
	for(int i = static_cast<int>(currentCameraPoint); i <static_cast<int>(distanceToPoint.size()); i++)
	{
		if (currentDistance <= distanceToPoint[i])
		{
			index = i - 1;
			break;
		}
	}
	if (index == -1)
	{
		currentCameraPoint = cameraPoints.size() + 1;
	}
	else
	{
		float dist = currentDistance - distanceToPoint[index];
		float splinePart = dist / distanceToNext[index];
		currentCameraPoint = index + splinePart;
	}
	return currentCameraPoint;
}

//--------------------------------------------------------------
void ofApp::update() {
	// Update delta time & lastFrame
	chrono::system_clock::time_point temp = chrono::system_clock::now();
	deltaTime = static_cast<chrono::duration<float>>(temp - lastFrame).count();
	lastFrame = temp;

	if (!movingCamera)
	{
		// Set Camera Points Mode

		// Controls
		cameraPosition = cam.getPosition();
		if (myKeyHandler.IsKeyDown('w')) 
		{
			cameraPosition.z -= kMoveInc * cosf(ofDegToRad(cameraRotation.x)) * deltaTime;
			cameraPosition.x -= kMoveInc * sinf(ofDegToRad(cameraRotation.x)) * deltaTime;
		}
		if (myKeyHandler.IsKeyDown('s'))
		{
			cameraPosition.z += kMoveInc * cosf(ofDegToRad(cameraRotation.x)) * deltaTime;
			cameraPosition.x += kMoveInc * sinf(ofDegToRad(cameraRotation.x)) * deltaTime;
		}
		if (myKeyHandler.IsKeyDown('a'))
		{
			cameraPosition.z += kMoveInc * sinf(ofDegToRad(cameraRotation.x)) * deltaTime;
			cameraPosition.x -= kMoveInc * cosf(ofDegToRad(cameraRotation.x)) * deltaTime;
		}
		if (myKeyHandler.IsKeyDown('d'))
		{
			cameraPosition.z -= kMoveInc * sinf(ofDegToRad(cameraRotation.x)) * deltaTime;
			cameraPosition.x += kMoveInc * cosf(ofDegToRad(cameraRotation.x)) * deltaTime;
		}
		if (myKeyHandler.IsKeyDown('q'))
		{
			cameraPosition.y += kMoveInc * deltaTime;
		}
		if (myKeyHandler.IsKeyDown('e'))
		{
			cameraPosition.y -= kMoveInc * deltaTime;
		}
		cam.setPosition(cameraPosition);

		// Set spline points
		if (myKeyHandler.WasKeyReleased('f'))
		{
			cameraPoints.push_back({ { cam.getPosition() }, cam.getGlobalOrientation() });
		}

		// Clear all spline points
		if(myKeyHandler.WasKeyReleased('c'))
		{
			cameraPoints.clear();
		}
	}
	else
	{
		// Moving Camera Mode

		// Camera Spline
		float k = doStep(deltaTime * speed);
		CameraPoint myCameraPoint = interpolation((int)k, fmodf(k, 1.0));
		currentCameraPoint++;
		cam.setPosition(myCameraPoint.position.x, myCameraPoint.position.y, myCameraPoint.position.z);
		cam.setGlobalOrientation(myCameraPoint.rotation);
	}

	// Switch between camera moving and point setting modes
	if (myKeyHandler.WasKeyReleased('g'))
	{
		if (movingCamera)
		{
			movingCamera = false;
		}
		else
		{
			movingCamera = true;
			reset();
		}
		lastFrame = chrono::system_clock::now();
	}

	// Change speed
	if (myKeyHandler.WasKeyReleased('+'))
	{
		speed += 10;
	}
	if (myKeyHandler.WasKeyReleased('-'))
	{
		speed -= 10;
	}

	// Animate earth
	earth.panDeg(1);

	// LOD Switching
	size = sizeof(tree) / sizeof(tree[0]);
	for (int i = 0; i < size; i++) 
	{
		tree[i].switchLOD(cam);
	}

	// Billboards
	for (auto i = 0; i < numOfClouds; i++) 
	{
		clouds[i].lookAt(cam);
	}

	// Transparency sorting
	int chaos = sizeof(chaosCube) / sizeof(chaosCube[0]);
	int order = sizeof(orderCube) / sizeof(orderCube[0]);
	bubbleSort(chaosCube, transparentChaosColor, chaos);
	bubbleSort(orderCube, transparentOrderColor, order);

	if (clashCounter == 40)
	{
		countdown = true;
	}
	else if (clashCounter == 0)
	{
		countdown = false;
	}
	if (countdown)
	{
		sword1.setPosition(sword1.getPosition().x, sword1.getPosition().y, sword1.getPosition().z - 0.5);
		sword2.setPosition(sword2.getPosition().x, sword2.getPosition().y, sword2.getPosition().z + 0.5);
		dove.setPosition(dove.getPosition().x, dove.getPosition().y + 0.05, dove.getPosition().z);
		moveEarth.setPosition(moveEarth.getPosition().x, moveEarth.getPosition().y + 0.2, moveEarth.getPosition().z);
		clashCounter--;
	}
	else
	{
		sword1.setPosition(sword1.getPosition().x, sword1.getPosition().y, sword1.getPosition().z + 0.5);
		sword2.setPosition(sword2.getPosition().x, sword2.getPosition().y, sword2.getPosition().z - 0.5);
		dove.setPosition(dove.getPosition().x, dove.getPosition().y - 0.05, dove.getPosition().z);
		moveEarth.setPosition(moveEarth.getPosition().x, moveEarth.getPosition().y - 0.2, moveEarth.getPosition().z);
		clashCounter++;
	}
	myKeyHandler.FrameUpdate();
}

//--------------------------------------------------------------
void ofApp::draw() {

	// Render scene from the position of the first light
	myShadow.StartDepthPass(0);
	glEnable(GL_DEPTH_TEST);
	renderScene();
	glDisable(GL_DEPTH_TEST);
	myShadow.EndDepthPass(0);

	// Render scene from the position of the second light
	myShadow.StartDepthPass(1);
	glEnable(GL_DEPTH_TEST);
	renderScene();
	glDisable(GL_DEPTH_TEST);
	myShadow.EndDepthPass(1);

	// Render scene from the main camera
	myShadow.StartRenderPass(cam);
	cam.begin();
	glEnable(GL_DEPTH_TEST);
	renderScene();
	glDisable(GL_DEPTH_TEST);
	cam.end();
	myShadow.EndRenderPass();

}

void ofApp::renderScene() {

	ofSetColor(255, 255, 255);

	// Earth
	//earthTex.bind();
	myShadow.GetShader().setUniformTexture("tex0", earthTex, 1);
	earth.draw();
	stillEarth.draw();
	moveEarth.draw();
	//earthTex.unbind();

	// Trees
	//for (int i = 0; i < size; i++)
	//{
	//	tree[i].currentModel.drawFaces();
	//}

	// Pillar
	//pillar.drawFaces();

	// Dove
	//dove.drawFaces();

	// Swords
	//sword1.drawFaces();
	//sword2.drawFaces();

	//// OrnamentCube without Mipmapping
	//ornamentCube.setScale(0.3, 0.3, 0.3);
	//ornamentCube.setPosition(-50, 15, -50);
	////ornamentTex.bind();
	//myShadow.GetShader().setUniformTexture("tex0", ornamentTex, 1);
	//ornamentCube.draw();
	////ornamentTex.unbind();

	//// OrnamentCube with Mipmapping
	//ornamentCube.setScale(0.3, 0.3, 0.3);
	//ornamentCube.setPosition(30, 15, -30);
	////mipmapTex.bind();
	//myShadow.GetShader().setUniformTexture("tex0", mipmapTex, 1);
	//ornamentCube.draw();
	////mipmapTex.unbind();

	// Ground
	//grassTex.bind();
	myShadow.GetShader().setUniformTexture("tex0", grassTex, 1);
	ground.draw();
	//grassTex.unbind();

	//ofEnableAlphaBlending();

	//// Clouds
	//cloudTex.bind();
	//for (auto cloud : clouds)
	//{
	//	cloud.draw();
	//}
	//cloudTex.unbind();

	// Transparent cubes

	//for (int i = 0; i < 6; i++)
	//{
	//	ofSetColor(transparentChaosColor[i]);
	//	chaosCube[i].draw();
	//}
	//for (int i = 0; i < 6; i++)
	//{
	//	ofSetColor(transparentOrderColor[i]);
	//	orderCube[i].draw();
	//}
	//ofDisableAlphaBlending();
	//ofClearAlpha();
}

//--------------------------------------------------------------
void ofApp::bubbleSort(ofPlanePrimitive* cubeSide, ofColor* color, int counter) {
	int i, j;
	for (i = 0; i < counter - 1; i++) 
	{
		for (j = 0; j < counter - i - 1; j++) 
		{
			ofVec3f cPos1 = cubeSide[j].getPosition();
			ofVec3f cPos2 = cubeSide[j + 1].getPosition();
			if (cPos1.squareDistance(cam.getPosition()) < cPos2.squareDistance(cam.getPosition())) 
			{
				std::swap(cubeSide[j], cubeSide[j + 1]);
				std::swap(color[j], color[j + 1]);
			}
		}
	}
}

//--------------------------------------------------------------
float ofApp::clamp(float n, float min, float max) {
	return (n < min) ? min : (max < n) ? max : n;
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {
	myKeyHandler.KeyPressed(key);
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {
	myKeyHandler.KeyReleased(key);
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {
	if (!movingCamera)
	{
		float middleY = ofGetHeight() / 2;
		float middleX = ofGetWidth() / 2;
		cam.setOrientation({ 0,0,0 });
		cameraRotation.y += fmod((middleY - y) / 10, 360);
		cameraRotation.x += fmod((middleX - x) / 10, 360);
		cameraRotation.y = clamp(cameraRotation.y, -60, 89);
		cam.panDeg(cameraRotation.x);
		cam.tiltDeg(cameraRotation.y);
		glfwSetCursorPos(((ofAppGLFWWindow*)ofGetWindowPtr())->getGLFWWindow(), middleX, middleY);
	}
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}