#ifndef CAMERAPOINT_H
#define CAMERAPOINT_H

#include "ofMain.h"

class CameraPoint
{
public:
	CameraPoint();
	CameraPoint(ofVec3f pos, ofQuaternion rot);

	ofVec3f position;
	ofQuaternion rotation;
};

#endif // CAMERAPOINT_H
