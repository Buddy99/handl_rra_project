#pragma once

#include "ofMain.h"
#include "ofxAssimpModelLoader.h"
#include "Model.h"
#include "CameraPoint.h"
#include "GLFW/glfw3.h"
#include "glm/gtx/quaternion.hpp"
#include "KeyHandler.h"
#include "Shadow.h"
#include <chrono>

#define kMoveInc 250
#define kRotInc 5
#define numOfClouds 40
#define accuracy 0.01

class ofApp : public ofBaseApp {
public:
	// Main functions
	void setup();
	void update();
	void draw();
	void renderScene();

	// Input Functions
	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void mouseEntered(int x, int y);
	void mouseExited(int x, int y);

	// Helper Functions
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);

	// CGSE Mystic Forest
	void setupTree(Model& model, float x, float y, float z);
	void setupModels();
	void bubbleSort(ofPlanePrimitive* cubeSide, ofColor* color, int counter);
	float clamp(float n, float min, float max);

	// EZG UE1
	void reset();
	float doStep(float s);
	CameraPoint interpolation(int currentPoint, float k);
	void initializeCameraPoints();

	//--------------------------------------------------------------------------

	ofSpherePrimitive earth;
	ofSpherePrimitive stillEarth;
	ofSpherePrimitive moveEarth;
	ofPlanePrimitive ground;
	ofBoxPrimitive ornamentCube;
	ofPlanePrimitive clouds[numOfClouds];

	ofLight cornerLights[2];
	ofTexture earthTex;
	ofTexture grassTex;
	ofTexture ornamentTex;
	ofTexture mipmapTex;
	ofTexture cloudTex;

	ofxAssimpModelLoader sword1;
	ofxAssimpModelLoader sword2;
	ofxAssimpModelLoader pillar;
	ofxAssimpModelLoader dove;
	Model tree[19];
	
	int size;
	int clashCounter = 0;
	bool countdown = true;

	ofTrueTypeFont	chosenSide;
	string side;
	ofRectangle bounds;
	ofPlanePrimitive chaosCube[6];
	ofPlanePrimitive orderCube[6];
	ofColor transparentChaosColor[6];
	ofColor transparentOrderColor[6];

	bool keys[255];
	KeyHandler myKeyHandler;

	// Interpolation variables
	float t = 0;	// Tension
	float b = 0;	// Bias
	float c = 0;	// Continuity
	float d0a;
	float d0b;
	float d1a;
	float d1b;

	// Camera variables
	bool movingCamera = false;
	chrono::system_clock::time_point lastFrame;
	float deltaTime;
	ofCamera cam;
	ofVec3f cameraPosition;
	ofVec3f cameraRotation;
	ofVec2f mousePosition;
	std::vector<CameraPoint> cameraPoints;
	std::vector<float> distanceToNext;
	std::vector<float> distanceToPoint;
	float currentDistance = 0;
	float currentCameraPoint = 0;
	float speed = 50;

	Shadow myShadow;
};
