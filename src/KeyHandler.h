#pragma once

#include <memory>
const int NUM_KEYS = 255;

class KeyHandler
{
public:
	KeyHandler();
	~KeyHandler();

	void KeyPressed(int key);
	void KeyReleased(int key);
	void FrameUpdate();

	bool IsKeyDown(char key);
	bool WasKeyReleased(char key);
	bool WasKeyPressed(char key);

protected:
	bool mKeyDown[NUM_KEYS];
	bool mKeyReleased[NUM_KEYS];
	bool mKeyPressed[NUM_KEYS];
};

typedef std::unique_ptr<KeyHandler> KeyHandlerPtr;
