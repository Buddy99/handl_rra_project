#include "Shadow.h"

Shadow::Shadow()
: width(ofGetWidth())
, height(ofGetWidth())
, biasMatrix(0.5, 0.0, 0.0, 0.0,
    0.0, 0.5, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.5, 0.5, 0.5, 1.0)
{
    for (int i = 0; i < 2; i++)
    {
        string iAsString = to_string(i);
        lightCam[i] = make_pair("uShadowLightPos" + iAsString, ofCamera());
        depthBias[i] = make_pair("uShadowBias" + iAsString, 0.01);
        shadowIntensity[i] = make_pair("uShadowIntensity" + iAsString, 0.75);
        shadowFramebufferobject[i] = make_pair("uShadowMap" + iAsString, ofFbo());
        shadowTransMatrix[i] = make_pair("uShadowTransMatrix" + iAsString, ofMatrix4x4());
        SetFramebufferobjectSettings(i);
    }
}

const ofShader& Shadow::GetShader() const
{
    return myShader;
}

void Shadow::LoadShader(string path)
{
    // Load vertex shader & fragment shader
    myShader.load(path);
}

void Shadow::SetShadowClipRange(int index, float nearClip, float farClip)
{
    //Set near plane + far plane for each light camera 
    lightCam[index].second.setNearClip(nearClip);
    lightCam[index].second.setFarClip(farClip);
}

void Shadow::SetLightCamPosition(int index, ofVec3f lightPos)
{
    lightCam[index].second.setPosition(lightPos);
}

void Shadow::SetLightCamLookAt(int index, ofVec3f lookAtPos, ofVec3f lookUpVector)
{
    lightCam[index].second.lookAt(lookAtPos, lookUpVector);
}

// Starts rendering the depth information of the scene
void Shadow::StartDepthPass(int index)
{
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
    
    shadowFramebufferobject[index].second.begin();
    ofClear(255);
    lightCam[index].second.begin();
}

// Ends rendering the depth information of the scene
void Shadow::EndDepthPass(int index)
{
    lightCam[index].second.end();
    shadowFramebufferobject[index].second.end();
    
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
}

void Shadow::StartRenderPass(ofCamera &cam)
{
    myShader.begin();
    SetShaderData(cam);
}

void Shadow::EndRenderPass()
{
    myShader.end();
}

void Shadow::SetShaderData(ofCamera& cam)
{
    // For each light
    for (int i = 0; i < 2; i++)
    {
        // Set Texture data in shader
        myShader.setUniformTexture(shadowFramebufferobject[i].first, shadowFramebufferobject[i].second.getDepthTexture(), 0);

        ofMatrix4x4 inverseCameraMatrix = ofMatrix4x4::getInverseOf(cam.getModelViewMatrix());
        shadowTransMatrix[i].second = inverseCameraMatrix * lightCam[i].second.getModelViewMatrix() * lightCam[i].second.getProjectionMatrix() * biasMatrix;

        // Set Shader data;
        myShader.setUniformMatrix4f(shadowTransMatrix[i].first, shadowTransMatrix[i].second);
        myShader.setUniform1f(depthBias[i].first, depthBias[i].second);
        myShader.setUniform1f(shadowIntensity[i].first, shadowIntensity[i].second);
        myShader.setUniform3f(lightCam[i].first, lightCam[i].second.getPosition());
    }
}

void Shadow::SetFramebufferobjectSettings(int index)
{
    //Framebufferobject settings
    ofFbo::Settings settings;
    settings.width = width;
    settings.height = height;
    settings.textureTarget = GL_TEXTURE_2D;
    settings.internalformat = GL_RGBA32F_ARB;
    settings.useDepth = true;
    settings.depthStencilAsTexture = true;
    settings.useStencil = true;

    // allocate memory for the framebufferobject
    shadowFramebufferobject[index].second.allocate(settings);
}
