#pragma once
#include "ofMain.h"

class Shadow {
public:
    
    Shadow();

    const ofShader& GetShader() const;
    void LoadShader(string path);
    void SetShadowClipRange(int index, float nearClip, float farClip);

    void SetLightCamPosition(int index, ofVec3f lightPos);
    void SetLightCamLookAt(int index, ofVec3f lookAtPos, ofVec3f lookUpVector = ofVec3f(0, 1, 0));
    
    void StartDepthPass(int index);
    void EndDepthPass(int index);  
    void StartRenderPass(ofCamera& cam);
    void EndRenderPass();

protected:
    void SetShaderData(ofCamera& cam);

    void SetFramebufferobjectSettings(int index);
    
    float width;
    float height;
    ofMatrix4x4 biasMatrix;

    pair<string, ofCamera> lightCam[2];
    pair<string, float> depthBias[2];
    pair<string, float> shadowIntensity[2];
    pair<string, ofFbo> shadowFramebufferobject[2];
    pair<string, ofMatrix4x4> shadowTransMatrix[2];

    ofShader myShader;
};
