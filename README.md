# Handl_RRA_Project
Realtime Rendering and Algorithms Project - Anja Handl  
  
1. Projekt von Github herunterladen  
2. Openframeworks runterladen und entpacken  
3. Projekt im Ordner \of_v0.11.0_vs2017_release\apps\myApps entpacken  
(Der Ordner "myApps" sollte nun die beiden Ordner "emptyExample" und "handl_rra_project-master" enthalten)  
4. Solution "MysticForest.sln" im Ordner "handl_rra_project-master" starten und compilieren  
Alternativ: "MytsicForest_debug.exe" im "bin" Ordner starten  
  
Steuerung  
G - Zwischen "Kamera Punkte setzen" und "Kamerafahrt" Modus wechseln  
ESC - Programm beenden  
  
Kamera Punkte setzen Modus:  
C - alle bisher gespeicherten Punkte löschen  
F - Punkt mit Blickrichtung speichern  
W, A, S, D, Q, E - Bewegung  
  
Kamerafahrt Modus:  
'+' - Geschwindigkeit um 10 erhöhen  
'-' - Geschwindigkeit um 10 verringern  
(Die Kamerafahrt startet automatisch, sobald man in diesen Modus mit G wechselt und davor Punkte gespeichert wurden)